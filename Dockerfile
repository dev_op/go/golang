FROM golang:latest

RUN mkdir /main
ADD /application/. /main
WORKDIR /main
RUN go env -w GO111MODULE='on' && go build server.go
EXPOSE 80
CMD ["go", "run", "server.go"]