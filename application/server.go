package main

import (
  "fmt"
  "log"
  "html/template"
  "net/http"
  "time"
)

var tpl = template.Must(template.ParseFiles("index.html"))
var tplp = template.Must(template.ParseFiles("about.html"))

func appHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("got / request\n")
    // 	io.WriteString(w, "This is my website!\n")
  fmt.Println(time.Now(), "Hello from my new fresh server")
  tpl.Execute(w, nil)
}

func getAbout(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("got /about request\n")
    // 	io.WriteString(w, "About page, HTTP!\n")
  fmt.Println(time.Now(), "Hello from my new fresh server | About")
  tplp.Execute(w, nil)
}

func main() {
  http.HandleFunc("/", appHandler)
  http.HandleFunc("/about", getAbout)

  log.Println("Started, serving on port 80")
  err := http.ListenAndServe(":80", nil)

  if err != nil {
		fmt.Printf("error starting server: %s\n", err)
    log.Fatal(err.Error())
  }
}